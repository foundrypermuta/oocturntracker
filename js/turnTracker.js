class OutOfCombatTurnTracker {

    static async Init(controls, html) {

        const diceRollbtn = $(
            `
            <li class="scene-control sdr-scene-control" data-control="oocTurnTracker" title="Out of combat turn tracker">
                <i class="fas fa-chess"></i>
            
                <ol class="control-tools">
                    
                </ol>
            </li>
            `
        );

        html.append(diceRollbtn);

        diceRollbtn[0].addEventListener('click', ev => this.openTurnTracker(ev));

    }

    static openTurnTracker = (event) => {
        console.debug("Abriendo turnTracker")
        let oTTsheet = new OocTurnTrackerSheet(); 
        oTTsheet._renderMe()


    }
}

class TurnModel{
    actTurn = 0

    nextTurn = (n) => {
        this.actTurn += n;
    }
}

class OocTurnTrackerSheet extends Application {
    
    turnModel = new TurnModel() 
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            title: "Out of combat Turn Tracker",
            id: "client-settings",
            template: "/modules/out-of-combat-turn-tracker/templates/OocTurnTracker.hbs",
            width: 600,
            height: 900,
            tabs: [
                { navSelector: ".tabs", contentSelector: ".content", initial: "core" }
            ]
        })
    }

    getData(options = {}){
        return this.turnModel
    }

    _renderMe(){
        this.render(true, this.turnModel)
        
    }

    activateListeners(html){
        let that = this;

        super.activateListeners(html)
        html.find("#nextTurnBtn").click(() => {
            console.debug("click")
            that.turnModel.nextTurn(1)
            that._renderMe(true, this.turnModel);
        })
    }
}



Hooks.on('renderSceneControls', (controls, html) => { OutOfCombatTurnTracker.Init(controls, html); });

Hooks.once("init", () => {

});

console.log("SDR | Simple Dice Roller loaded");